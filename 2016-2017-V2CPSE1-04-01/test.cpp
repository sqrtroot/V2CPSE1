#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "string.h"
#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
TEST_CASE( "Test constructors & c_str function", "[String]" ) {
    String<20> noArgs = String<20>();
    String<20> charPArg = String<20>((char *)"abcdefg");
    String<20> stringArg= String<20>(std::string("ABCDEFG"));

    REQUIRE(strcmp(noArgs.c_str(),"\0") == 0);
    REQUIRE(strcmp(charPArg.c_str(),"abcdefg") == 0);
    REQUIRE(strcmp(stringArg.c_str(),"ABCDEFG") == 0);
};

TEST_CASE("Test length function","[String]"){
    String<20> normalString = String<20>("abcdefg");
    String<20> withNullString = String<20>("abcdefg\0abcdef");

    REQUIRE(normalString.length()==7);
    REQUIRE(withNullString.length()==7);

};

TEST_CASE("Test overflow","[String]"){
    String<5> overflowCharString = String<5>((char *)"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrtapsodijpfoijdsaf");
    String<5> overflowStringString = String<5>(std::string("abcdefgaosjdpfijsapdoijfposaidjfpoijsadpofjzkxjpijfas"));

    REQUIRE(overflowCharString.length()==5);
    REQUIRE(overflowStringString.length() == 5);
};

TEST_CASE("operator =","[String]"){
    String<5> string1("abc");
    std::string string2("2");
    
    string1 = 'a';
    REQUIRE(strcmp(string1.c_str(),(const char*)"a") == 0);

    string1 = string2.c_str();
    REQUIRE(strcmp(string1.c_str(),(const char*)"2")==0);

    string1 = string2;
    REQUIRE(strcmp(string1.c_str(),(const char*)"2")==0);
};

TEST_CASE("operator +=","[String]"){
    String<5> string1("a");
    
    string1 += 'b';
    REQUIRE(strcmp(string1.c_str(),(const char*)"ab")==0);
    
    string1+=(const char*)"cd";
    REQUIRE(strcmp(string1.c_str(),(const char*)"abcd")==0);

    string1+=std::string("e");
    REQUIRE(strcmp(string1.c_str(),(const char*)"abcde")==0);

    string1+=std::string("fghijklmnop");
    REQUIRE(strcmp(string1.c_str(),(const char*)"abcde")==0);
};

TEST_CASE("operator []","[String]"){
    String<5> string1("abcde");

    const char a = string1[0];
    REQUIRE(a == 'a');
    const char b = string1[1];
    REQUIRE(b == 'b');
    
    char c = string1[2];
    REQUIRE(c == 'c');
    c = 'd';
    REQUIRE(c == 'd');
    REQUIRE(strcmp(string1.c_str(),(const char*)"abcde")==0);
};

TEST_CASE("operator <<","[String]"){
    String<5> string1("abcde");
    std::ostringstream stream;
    stream << string1;
    REQUIRE(stream.str() == "abcde");
};