#include "hwlib.hpp"
#include "clock.hpp"
#include "math.h"
int main(void)
{

    // kill the watchdog
    WDT->WDT_MR = WDT_MR_WDDIS;

    // wait for the PC console to start
    hwlib::wait_ms(500);

    namespace target = hwlib::target;

    auto scl = target::pin_oc(target::pins::scl);
    auto sda = target::pin_oc(target::pins::sda);

    auto i2c_bus = hwlib::i2c_bus_bit_banged_scl_sda(sda, scl);
    auto pin_gnd = target::pin_out(target::pins::d19);
    pin_gnd.set(1);
    auto pin_vcc = target::pin_out(target::pins::d18);
    pin_vcc.set(0);

    auto oled = hwlib::glcd_oled_buffered(i2c_bus, 0x3c);

    hwlib::cout << "Running\n";
    auto clock = Clock(oled);
    while (1)
    {
        for (int i = 0; i <= 60 * 12; i++)
        {
            hwlib::cout << "hours: " << (int) floor(i/60) << " minutes: " << i % 60 << "\n";
            clock.draw(floor(i / 60), i % 60);
            oled.flush();
            // hwlib::wait_ms(1);
            oled.clear();
        }
    }
    hwlib::cout << "done\n";
}
