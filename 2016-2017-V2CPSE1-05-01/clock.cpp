#include "clock.hpp"

void Clock::drawPlate()
{
    hwlib::circle(hwlib::location(display.size.x / 2, display.size.y / 2), 30).draw(display);
};

void Clock::drawMinute(int degrees)
{
    float rads = degrees * 0.0174532925;
    int x = display.size.x / 2 + 30 * cos(rads);
    int y = display.size.y / 2 + 30 * sin(rads);
    hwlib::line(hwlib::location(display.size.x / 2, display.size.y / 2), hwlib::location(x, y)).draw(display);
};

void Clock::drawHour(int degrees)
{
    float rads = degrees * 0.0174532925;
    int x = display.size.x / 2 + 25 * cos(rads);
    int y = display.size.y / 2 + 25 * sin(rads);
    hwlib::line(hwlib::location(display.size.x / 2, display.size.y / 2), hwlib::location(x, y)).draw(display);
};

void Clock::draw(double h, double m)
{
    // validate the input
    if (h == 12)
    {
        h = 0;
    }
    if (m == 60)
    {
        m = 0;
    }
    drawPlate();
    drawHour(((60 * h) + m) * degreesPerHourMinute);
    drawMinute(m * degreesPerMinute);
};

Clock::Clock(hwlib::window &window) : display(window){};