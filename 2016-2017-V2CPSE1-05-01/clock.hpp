#include "hwlib.hpp"
#include "math.h"

class Clock
{
  private:
    hwlib::window &display;
    void drawPlate();
    void drawMinute(int degrees);
    void drawHour(int degrees);
    const float degreesPerHourMinute = .5;
    const float degreesPerMinute = 6;

  public:
    Clock(hwlib::window &window);
    void draw(double h, double m);
};